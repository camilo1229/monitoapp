class User < ApplicationRecord
  include BCrypt

  ADMIN = 1
  ENGINEER = 2

  validates_presence_of :email, :name, :last_name
  validates_presence_of :password
  validates_confirmation_of :password
  validates_presence_of :password_confirmation, if: :password_changed?

  before_create :encrypt_password, if: Proc.new { |a| !a.password.blank? }
  # before_update :encrypt_password, if:  Proc.new { |a| !a.reset_password_token.blank? }


  def validate_password(insecure)
    self.password.blank? ? false : Password.new(self.password) == insecure
  end

  def details

  end

  private
    def encrypt_password
      self.password = Password.create(password)
    end

end

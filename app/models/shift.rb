class Shift < ApplicationRecord
  belongs_to :service
  has_many :schedules

  after_create :set_shift_schedules

  def details
    {
      name: name, date_range: "#{start_date} -  #{end_date}", status: status, schedules: schedules.group_by {|x| x.day_name}
    }
  end

  private
    # TODO with sidekiq
    def set_shift_schedules
      ShiftScheduleService.create_shift_schedules(self)
    end

end

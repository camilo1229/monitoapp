class SetSchedule
  CALENDAR = JSON.parse(File.read('calendar.json'))

  def self.set(schedules)
    sch = {}
    schedules.each do |k,v|
      sch[k] = { day: CALENDAR["days"][k.to_i], times: self.hours_details(v.sort) }
    end
    return sch
  end

  def self.hours_details(times)
    hours = CALENDAR["hours"]
    det = {}
    times.each do |time|
      det[time.to_s] = {label: hours[time]} 
    end
    return det
  end
end

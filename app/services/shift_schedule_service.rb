class ShiftScheduleService

  def self.create_shift_schedules(shift)
    sche = shift.service.schedules
    list = []
    sche.each do |day, times|
      day_name = times["day"]
      times["times"].each do |k|
        list << { day: day.to_i, day_name: day_name, hour: k[0].to_i, hour_name: k[1]["label"]}
      end
    end
    shift.schedules.create(list)
  end
end
class ApiController < ActionController::API
  protected
    def authenticate
      if request.headers.include?('token')
        @token_decode = Auth.decode_token(request.headers['token'].split(" ")[1])
        unless @token_decode.status == :ok
          return render json: { message: @token_decode.message }, status: @token_decode.status
        end
        unless current_user
          return render json: { message: "User not found" }, status: :not_found
        end
      else
        render json: { message: "Token is required" }, status: :bad_request
      end
    end

    def current_user
      if @current_user = User.find_by(id: @token_decode.data)
        return true
      end
      false
    end

    def validate_schedules
      if params.has_key?(:schedules) && !params[:schedules].blank?
        @schedules = params[:schedules]
        days_error = @schedules.keys.map(&:to_i) - [0,1,2,3,4,5,6]
        hours_error = @schedules.values.flatten - [* 0..23]
        if !days_error.blank?
          return render json: { days: days_error.map {|d| {day: d, error: "The day is not valid"}}, message: "Invalid days"}, 
                        status: :not_acceptable
        end
        if !hours_error.blank?
          return render json: { times: hours_error.map {|d| {time: d, error: "The time is not valid"}}, message: "Invalid Time"}, 
                        status: :not_acceptable
        end

  
      else
        return render json: {message: "Schedules is required and can not be blank or null" }, status: :unprocessable_entity
      end
    end

end
class Api::UsersController < ApiController
  before_action :authenticate
  before_action :validate_schedules, only: [:schedules]
  
  def update
    if @current_user.update_attributes(user_params)
      render json: { user: @current_user.profile_details, message: 'Updated user info' }
    else
      render json: { message: @current_user.errors.full_messages.to_sentence }, status: :unprocessable_entity
    end
  end

  def schedules
    @current_user.schedules = SetSchedule.set(@schedules)
    if @current_user.save
      render json: { schedules: @current_user.schedules , message: 'Set user schedules' }
    else
      render json: { message: "Error" }, status: :unprocessable_entity
    end
    
  end

  def my_schedules
   render json: { schedules: @current_user.schedules}
  end

  private
    def user_params
      params.require(:user).permit(:email, :name, :last_name)
    end
end
class Api::ShiftsController < ApiController
  before_action :authenticate
  before_action :set_shift, only: [:show]
  
  def show
    render json: {
      shift: @shift.details
    }, status: :ok
  end
  def my_shfits
    render json: { shifts: Shift.actives.where(user_id: @current_user.id).collect(&:details) }
  end
  private
   def set_shift
      if !@shift = Shift.includes(schedules: :user).find_by(id: params[:id])
        render json: {message: "Shift not found"}, status: :not_found
      end

   end
end
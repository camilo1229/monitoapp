class Api::SessionsController < ApiController
  
  # Autor: Juan Camilo
  #
  # Autor actualizacion: November 19, 2018
  #
  # Fecha actualizacion: 
  #
  # Metodo: Post - Iniciar sesión con email y contraseña
  #
  # URL: api/login
  #
  # Resultado: Si las credenciales de acceso son correctas retorna datos usuario
  # + token, sino mensajes indicando el error.
  #
  def login
    if user = User.find_by(email: params[:email])
      if user.validate_password(params[:password])
          render json: { 
            user: user.attributes.slice("id","name", "last_name", "email", "role"),
            token: Auth.create_token(user.id), 
            message: "Login success"
            }, status: :ok
      else
        render json: { message: "Invalid password" }, status: :forbidden
      end
    else
      render json: {message: "User not found"}, status: :not_found
    end
  end

end

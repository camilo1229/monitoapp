class Api::ServicesController < ApiController
  before_action :authenticate
  before_action :admin
  before_action :validate_schedules, only: [:create]

  def create
    @service = Service.new(service_params)
    @service.schedules = SetSchedule.set(@schedules)
    #TODO : Fix with sidekiq
    @service.shifts.build([
      {name: "Semana 1", start_date: @service.start_date, end_date: @service.start_date.end_of_week},
      {name: "Semana 2", start_date: (@service.start_date + 1.week).beginning_of_week, end_date: (@service.start_date + 1.week).end_of_week },
      {name: "Semana 3", start_date: (@service.start_date + 2.week).beginning_of_week, end_date: (@service.start_date + 2.week).end_of_week },
      {name: "Semana 4", start_date: (@service.start_date + 3.week).beginning_of_week, end_date: (@service.start_date + 3.week).end_of_week },
      {name: "Semana 5", start_date: (@service.start_date + 4.week).beginning_of_week, end_date: (@service.start_date + 4.week).end_of_week }
    ])
    if @service.save
      render json: { service: @service, message: "Service created"}, status: :ok
		else
			render json: { message: @service.errors.full_messages.to_sentence}, status: :unprocessable_entity
		end
  end

  private
    def service_params
      params.require(:service).permit(:name, :start_date, :end_date)
    end
    def admin
      if @current_user.role != User::ADMIN
        return render json: { message: "Don´t have permissions"}, status: :unauthorized
      end
    end
end
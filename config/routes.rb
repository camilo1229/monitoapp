Rails.application.routes.draw do
  namespace :api do
    post 'login', to: 'sessions#login'

    resources :users, only: [:update] do
      collection do
        put 'schedules'
        get 'my_schedules'
      end
    end

    resources :services, only: [:create]
    resources :shifts, only: [:show] do
      collection do 
        get 'my_shifts'
      end
    end

  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end

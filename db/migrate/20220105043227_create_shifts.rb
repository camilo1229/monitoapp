class CreateShifts < ActiveRecord::Migration[7.0]
  def change
    create_table :shifts do |t|
      t.string :name, null: false, default: ''
      t.date :start_date
      t.date :end_date
      t.string :status
      t.references :service, null: false, foreign_key: true

      t.timestamps
    end
    add_index :shifts, :start_date
    add_index :shifts, :end_date
  end
end

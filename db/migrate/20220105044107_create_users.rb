class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :last_name
      t.string :email, null: false
      t.string :password
      t.integer :role, limit: 1, default: 2
      t.jsonb :schedules

      t.timestamps
    end
    add_index :users, :email
    add_index :users, :role
  end
end

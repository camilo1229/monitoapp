class CreateSchedules < ActiveRecord::Migration[7.0]
  def change
    create_table :schedules do |t|
      t.string :day_name
      t.integer :day, limit: 1
      t.integer :hour, limit: 1
      t.string :hour_name
      t.references :user, foreign_key: true
      t.references :shift, null: false, foreign_key: true

      t.timestamps
    end
    add_index :schedules, :day_name
    add_index :schedules, :hour_name
    add_index :schedules, :day
    add_index :schedules, :hour
  end
end

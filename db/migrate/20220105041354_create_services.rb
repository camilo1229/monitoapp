class CreateServices < ActiveRecord::Migration[7.0]
  def change
    create_table :services do |t|
      t.string :name, null: false, default: ''
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.string :status
      t.jsonb :schedules, default: {}

      t.timestamps
    end
    add_index :services, :name
    add_index :services, :status
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_05_045200) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "schedules", force: :cascade do |t|
    t.string "day_name"
    t.integer "day", limit: 2
    t.integer "hour", limit: 2
    t.string "hour_name"
    t.bigint "user_id"
    t.bigint "shift_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["day"], name: "index_schedules_on_day"
    t.index ["day_name"], name: "index_schedules_on_day_name"
    t.index ["hour"], name: "index_schedules_on_hour"
    t.index ["hour_name"], name: "index_schedules_on_hour_name"
    t.index ["shift_id"], name: "index_schedules_on_shift_id"
    t.index ["user_id"], name: "index_schedules_on_user_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.string "status"
    t.jsonb "schedules", default: {}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_services_on_name"
    t.index ["status"], name: "index_services_on_status"
  end

  create_table "shifts", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.date "start_date"
    t.date "end_date"
    t.string "status"
    t.bigint "service_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["end_date"], name: "index_shifts_on_end_date"
    t.index ["service_id"], name: "index_shifts_on_service_id"
    t.index ["start_date"], name: "index_shifts_on_start_date"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "email", null: false
    t.string "password"
    t.integer "role", limit: 2, default: 2
    t.jsonb "schedules"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email"
    t.index ["role"], name: "index_users_on_role"
  end

  add_foreign_key "schedules", "shifts"
  add_foreign_key "schedules", "users"
  add_foreign_key "shifts", "services"
end

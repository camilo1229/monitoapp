# README
Adjunto link de postman con las urls para consumir los servicios. https://www.getpostman.com/collections/e2f8349fc90d92204278

Se pueden crear usuarios de dos tipos, administradors e ingenieros configurando el atributo role del modelo user.
Se pueden asignar horario de disponibilidad a cada ingeniero.
Se pueden generar servicios y asignar los respectivos horarios de monitoreo

Se puede iniciar sessión, se implementa jwt para gestionar la sesión.
